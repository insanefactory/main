from .interface import Interface

class Action:
    """
    Base class for all Action classes. Actions are basic events of the system
    """
    def __init__(self, interface:Interface):
        self.interface = interface

    def run(self):
        raise NotImplementedError
    
    def get_action_name(self):
        return "EmptyAction"

class Macro(Action):
    def __init__(self):
        self.actions = []
    
    def append_action(self, action:Action):
        self.actions.append(action)
    
    def run(self):
        for action in self.actions:
            action.run()