from in_sane_factory import IN_SANE_FACTORY

if __name__ == "__main__":
    IN_SANE_FACTORY.load_config()
    IN_SANE_FACTORY.load_modules()

    print("Modules loaded:", ", ".join(IN_SANE_FACTORY.get_registered_modules()))
    print("Resources loaded:", IN_SANE_FACTORY.get_registered_resources())

    IN_SANE_FACTORY.load_app()
    IN_SANE_FACTORY.run()
