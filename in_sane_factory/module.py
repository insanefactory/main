from . import IN_SANE_FACTORY
from .layer_interface import Layer
from .base_io import IoInterface
from .serial import SerialInterface, SerialResource
from .support import SupportInterface

# TODO: Find way to init resources with cofiguration

class Module:
    ID = "default_module_id"
    ISF = IN_SANE_FACTORY
    INTERFACES = []
    RESOURCE = []

    def __init__(self) -> None:
        self.live_resources = []
        self.INTERFACES_DICT = {}
        for interface in self.INTERFACES:
            self.INTERFACES_DICT[interface.ID] = interface
        self.ISF.register(self)
    
    def init_resources(self, resources_settings={}):
        self.live_resources = []
        for resource in self.RESOURCE:
            if resource.ID in resources_settings:
                self.live_resources.append(
                    resource(**resources_settings.get(resource.ID))
                )
            else:
                print(f"[W] No config for {resource.ID} resource")
        return self.live_resources

    def create_interface(self, interface_id, if_args={}):
        interface = self.INTERFACES_DICT.get(interface_id)
        if not interface:
            raise Exception(f"[W] Unknown interface {interface_id}")
        if interface.REQUIRED_RESOURCES:
            if_args["resources"] = self.ISF.get_resources(interface.REQUIRED_RESOURCES)
        return interface(**if_args)

    def get_resource(self):
        return self.live_resources


class BaseModule(Module):
    ID = "base"
    INTERFACES = [Layer, IoInterface, SerialInterface, SupportInterface]
    RESOURCE = [SerialResource]

BaseModule()