from pathlib import Path
import json
import argparse
import shutil
from urllib.request import urlretrieve
from zipfile import ZipFile
import os


class Module:
    def __init__(self, moduel_path: Path) -> None:
        self.path = moduel_path
        self._load_config()

    def _load_config(self):
        with open(self.path / "module.json") as f:
            self.config = json.load(f)
            self.id = self.config["id"]

    def parse_libs(self):
        self.libs = self.config.get("required_libs", [])
        for lib in self.libs:
            if "source" not in lib:
                print("ERROR: Missing source for lib", lib)
                continue
            if lib["source"] == "link":
                name = Path(lib["url"]).name
                yield name, name
            else:
                yield f"{lib["source"]}/{lib["name"]}", lib["name"]

    def copy_to(self, target: Path, cache: Path):
        target_lib = target / self.id
        if target_lib.exists():
            shutil.rmtree(target_lib)
        shutil.copytree(self.path / self.id, target_lib)
        for lib_source, lib_target in self.parse_libs():
            source_lib_path = cache / lib_source
            if source_lib_path.is_dir():
                shutil.rmtree(target / lib_target, ignore_errors=True)
                shutil.copytree(source_lib_path, target / lib_target)
            elif source_lib_path.is_file():
                shutil.copy(source_lib_path, target / lib_target)
            elif (
                source_lib_path := source_lib_path.parent / f"{source_lib_path.name}.py"
            ).exists():
                shutil.copy(source_lib_path, target / source_lib_path.name)
            elif (source_lib_path := source_lib_path.with_suffix(".mpy")).exists():
                shutil.copy(source_lib_path, target / source_lib_path.name)
            else:
                print("ERROR: Missing lib", lib_source)


class Bob:  # the builder
    def __init__(self, target: Path, config: dict):
        self.target = target
        self.config = config
        self.required_modules = set()
        self.modules: dict[str:Module]
        self.download_path = Path("./cache/")

    def load_device(self):
        config_path = self.target / "config.json"
        if not config_path.exists():
            return False
        with open(config_path, "r") as f:
            config = json.load(f)
            interfaces = config.get("interfaces", {})
            for interface_name in interfaces.keys():
                self.required_modules.add(interfaces[interface_name]["module"])
        self.required_modules.remove("base")  # Base is always included
        return True

    def load_modules(self):
        folders = self.config.get("modulesFolders")
        if not folders:
            return
        modules: dict[str, Module] = {}
        for folder in folders:
            modules_path = Path(folder) / "modules"
            if modules_path.exists() and modules_path.is_dir():
                for directory in modules_path.iterdir():
                    index = (
                        directory / "module.json"
                    )  # This would like a bit nicer solution
                    if index.exists() and index.is_file():
                        module = Module(directory)
                        modules[module.id] = module
        self.modules = modules

    def resolve_dependencies(self):
        used_modules = {}
        for module_id in self.required_modules:
            if module_id in self.modules.keys():
                used_modules[module_id] = self.modules[module_id]
            else:
                print("ERROR: Unknown dependency", module_id)

        last_module_count = 0
        while last_module_count != len(used_modules):
            last_module_count = len(used_modules)
            keys = list(used_modules.keys())
            for module_key in keys:
                module = used_modules[module_key]
                for rm in module.config.get("required_modules", []):
                    if rm in used_modules:
                        continue
                    if rm in self.modules:
                        used_modules[rm] = self.modules[rm]
                        print("    Adding", rm)
                    else:
                        print("ERROR: Missing required module:", rm)

        return used_modules

    def collect_lib_dependencies(self, modules: list[Module]):
        sources = dict()
        for module_name, module in modules.items():
            for rm in module.config.get("required_libs", []):
                if "source" not in rm:
                    print("ERROR: Missing source for lib", rm)
                    continue
                elif rm["source"] == "link":
                    if "link" not in sources:
                        sources["link"] = {}
                    sources["link"][rm["type"]] = sources["link"].get(
                        rm["type"], []
                    ) + [rm["url"]]
                else:
                    sources[rm["source"]] = sources.get(rm["source"], []) + [rm["name"]]
        return sources

    def download_libs(self, libs: dict, clean: bool = False):
        self.download_path.mkdir(exist_ok=True)
        lib_sources = self.config.get("libraries", {})
        for source_name in libs.keys():
            if source_name == "link":
                for file_url in libs["link"]["file"]:
                    file_name = Path(file_url).name
                    print("Downloading", file_name)
                    target_name = self.download_path / file_name
                    if target_name.exists() and not clean:
                        print("    Already downloaded")
                        continue
                    urlretrieve(file_url, target_name)
            elif source_name in lib_sources:
                source = lib_sources[source_name]
                if "link" in source:
                    if (self.download_path / source_name).exists() and not clean:
                        print("    Already downloaded")
                        continue
                    file_name = Path(source["link"]).name
                    print("Downloading", source["link"])
                    urlretrieve(source["link"], self.download_path / file_name)
                    if source.get("type") == "zip":
                        with ZipFile(self.download_path / file_name, "r") as zip_ref:
                            zip_ref.extractall(self.download_path)
                        os.remove(self.download_path / file_name)
                        shutil.move(
                            self.download_path
                            / file_name.rstrip(".zip")
                            / source.get("root", "."),
                            self.download_path / source_name,
                        )
                        shutil.rmtree(self.download_path / file_name.rstrip(".zip"))
                    else:
                        print("ERROR: Unknown type", source["type"])
            else:
                print("ERROR: Unknown source", source_name)

    def build_modules(self, modules: list[Module] | dict[str:Module]):
        modules = modules if type(modules) is list else modules.values()
        target = self.target / "lib"
        if not target.exists():
            target.mkdir()
        for module in modules:
            module.copy_to(target, self.download_path)

    def copy_base(self, target: Path):
        target_lib = target / "in_sane_factory"
        if target_lib.exists():
            shutil.rmtree(target_lib)
        shutil.copytree(
            Path(__file__).parent.parent.parent / "in_sane_factory", target_lib
        )


class Supervisor:
    def _load_config(self):
        config_path = Path(__file__).parent / "config.json"
        with open(config_path, "r") as f:
            return json.load(f)

    def _parse_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("--target", type=str, help="Target device or folder")
        parser.add_argument(
            "-v", "--verbose", action="store_true", help="Verbose output"
        )
        return parser.parse_args()

    def run(self):
        config = self._load_config()
        args = self._parse_args()
        bob = Bob(Path(args.target), config)
        bob.load_device()
        # print(bob.required_modules)
        bob.load_modules()
        dep = bob.resolve_dependencies()
        libs = bob.collect_lib_dependencies(dep)
        bob.download_libs(libs)
        bob.build_modules(dep)
        bob.copy_base(Path(args.target) / "lib")
        print("Done!")


if __name__ == "__main__":
    Supervisor().run()
