from .action import Action
from .interface import Interface

# TODO: Remove Button class. May not be used later.

class Button:
    def __init__(self, interface:Interface, press_action:Action, release_action:Action = None):
        self.press = press_action
        self.interface = interface
        self.release = release_action

    def run_release(self):
        if self.release:
            self.release.run()

    def run_press(self):
        if self.press:
            self.press.run()

    def update_press_action(self, action:Action):
        self.press = action

    def update_release_action(self, action:Action):
        self.release = action
