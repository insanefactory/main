# InSaneFactory
## How to config?
Configuration that stands from two main files:
* `resources.json`
    * As name suggests, this file is for configuration of resources. Those settings are related mostly to hardware.
    * > Note: For now only I2C resource exists.
* `config.json`
    * There are most of the settings for your device. InSaneFactory parses this file and create all instances based on it.
    * Main parts are:
        * `interfaces`:
            * *Dict of [interfaces](#interface)*
            * Keys of dict are used to identify interface when defining actions. Interfaces are used to execute actions or trigger them.
        * `macros`:
            * *Dictionary of lists of [actions](#action)*
            * By key in dictionary can those macros be called. Actions are executed in order in that are in list. 
        * `startup_actions`:
            * *List of [actions](#action)*
            * Actions in this list are executed in order after InSaneFactory is started. 

### Interface
Every interface need to have:
* `module` - Refers to module ID of interface origin 
* `interface` - Refers to interface ID
In addition there are optional:
* `args` - Arguments for interface instance. 
    * Some interfaces requires specific settings to be created. Those arguments depends of interface.
* `actions` - List of actions and their triggers for interface. Every item in list needs:
    * `trigger_id`: unique identificator of action that based on interface determine trigger conditions.
    * `actions`: Definition of action, that will be triggered.

### Action:
Action is defined by dictionary made of:
* [`interface`](#interface): Target interface that creates and executes this action.
* `type`: Type of action. Every interface can have number of avaiable actions, this defines that type.
* `args`: Action can use or even require additional information for execution. Those are stored in dictionary here.