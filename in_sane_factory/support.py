from .interface import Interface
from .action import Action
from microcontroller import reset
import os


class SupportInterface(Interface):
    ID = "support"

    def create_action(self, type, params):
        if type=="debug":
            return DebugAction(self, params.get("msg"))
        if type=="usermode":
            return UnsetUsermode(self)
        if type=="hard-restart":
            return HardRestart(self)


class UnsetUsermode(Action):
    def run(self):
        os.remove("usermode")
        reset()


class HardRestart(Action):
    def run(self):
        reset()


class DebugAction(Action):
    def __init__(self, interface: SupportInterface, message: str):
        self.m = message
        super().__init__(interface)
    def run(self):
        print(f"[D] Debug Action: {self.m}")
    def get_action_name(self):
        return "Debug Action"