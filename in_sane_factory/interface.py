class Interface:
    """
    Main holder class for every component group
    """

    ID = "Empty"
    REQUIRED_RESOURCES = [] # Array of IDs of required resources


    def __init__(self):
        pass

    def get_triggers(self):
        """
        Slots for actions and types of triggers that will activate them.

        Return:
        tuple (<Trigger Type (str)>, <Trigger ID(str)>)
        """
        return []

    def get_actions(self):
        """
        Avaiable actions to be trigged.
        """
        return []

    def create_action(self, type, params):
        """
        Returs an Action instance.
        """
        raise Exception("Using create action.")

    def set_trigger(self, id, action):
        """
        Set action for interface.
        id : str
            Unique identificator to set an action
        action : Action
            Action to be performed when triggered.
        """
        pass

    def update(self):
        pass