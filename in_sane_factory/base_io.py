from .interface import Interface as BaseInterface
from .action import Action
import board
from digitalio import DigitalInOut, Direction


class IoInterface(BaseInterface):
    ID = "dio" # It's me! Dio!

    def __init__(self):
        super().__init__()
        self.pins = {}

    def get_actions(self):
        port_list = list(filter(lambda x: x[0] == "D" or x[:3] == "LED", dir(board)))
        return [f"{port}_off" for port in port_list] + [f"{port}_on" for port in port_list]
    
    def setPin(self, pin, value):
        name = str(pin)
        if name not in self.pins:
            pin = DigitalInOut(pin)
            pin.direction = Direction.OUTPUT
            self.pins[name] = pin
        self.pins[name].value = value

    def create_action(self, type, params):
        port = params.get("pin")
        if port and hasattr(board, port):
            action = None
            if type == "set_on":
                action = DigitalWriteOn
            if type == "set_off":
                action = DigitalWriteOff
            pin = getattr(board, port)
            return action(self, pin)


class DigitalWriteOn(Action):
    def __init__(self, interface: IoInterface, pin):
        if type(pin) is str:
            pass # TODO: Translate string to pin
        self.pin_id = pin
        super().__init__(interface)

    def run(self):
        # self.interface.
        self.interface.setPin(self.pin_id, True)

class DigitalWriteOff(DigitalWriteOn):
    def run(self):
        self.interface.setPin(self.pin_id, False)
