# https://docs.circuitpython.org/en/latest/shared-bindings/usb_cdc/index.html

import usb_cdc
import json
from .interface import Interface
from .action import Action
from .resource import ResourceBase
from microcontroller import reset
import os

class SerialInterface(Interface):
    ID = "serial_interface"
    REQUIRED_RESOURCES = ["SerialResource"]

    def __init__(self, resources=None):
        self.events = {}
        resource = resources.get("SerialResource")
        if not resource:
            raise Exception("Missing serial resource")
        self.resource = resource
        self.resource.register_interface(self)
        super().__init__()

    def send_event(self, name):
        self.resource.send(json.dumps({
            "event": name,
            "device": "default"
        }))
    
    def set_trigger(self, id, action:Action):
        self.events[id] = action
        print(f"[L] Event {id} added.")

    def _resolve_event(self, event_name, event_data):
        if event_name == "getEvents":
            self.resource.send(json.dumps({
                "event": "availableEvents",
                "device": "default",
                "data": list(self.events.keys()) + ["updateConfig", "endUsermode"]
            }))
        if event_name in self.events:
            self.events[event_name].run()
        else:
            print("[W] Unknown event", event_name, "recived.")
    
    def create_action(self, type, params):
        return SerialAction(self, type)

    def update(self):
        self.resource.update()
        return super().update()

class SerialResource(ResourceBase):
    ID = "SerialResource"
    UPDATEABLE = True

    def __init__(self):
        self.buffer = b''
        self.interface = None

    def getInterface(self):
        return self.interface

    def send(self, message):
        if not usb_cdc.data:
            print("[E] No data serial connected!")
            return
        usb_cdc.data.write(bytearray(message+"\n\r"))

    def resolve_message(self):
        try:
            data = json.loads(self.buffer.decode("utf-8"))
        except:
            print("[E] Can not read data: " + self.buffer.decode("utf-8"))
            return
        event_type = data.get("event")
        event_data = data.get("data")
        if event_type == "updateConfig":
            # TODO: Make this safer. ISF itself, loads new_config first and try to run it, than it ask via serial to 
            # keep it or rollback to original one.
            if event_data.get("newConfig"):
                with open("/config.json", "w") as config_file:
                    config_file.write(json.dumps(event_data["newConfig"]))
                reset()
        if event_type == "endUsermode":
            if "usermode" in os.listdir():
                os.remove("/usermode")
            reset()
        self.interface._resolve_event(event_name=event_type, event_data=event_data)

    def register_interface(self, interface:SerialInterface):
        # TODO: Add check to have only one serial interface or support multiple.
        self.interface = interface

    def update(self):
        if not usb_cdc.data:
            return
        while usb_cdc.data.in_waiting > 0:
            self.buffer += usb_cdc.data.read()
            if self.buffer[-1] in b'\n\r':
                self.resolve_message()
                self.buffer = b''


class SerialAction(Action):
    def __init__(self, interface: SerialInterface, event:str="noaction"):
        self.event = event
        super().__init__(interface)
    def run(self):
        self.interface.send_event(self.event)
