from os import listdir
import storage
import usb_cdc

usb_cdc.enable(console=True, data=True)    # Enable console and data
print("Serial port opened.")

if "usermode" in listdir():
    storage.disable_usb_drive()
    storage.remount("/", False)
elif "devmode" in listdir():
    storage.disable_usb_drive()
    storage.remount("/", False)
    storage.enable_usb_drive()
else:
    storage.enable_usb_drive()