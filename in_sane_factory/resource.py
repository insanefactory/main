class ResourceBase:
    """
    Resources are unique object that can have only one instance and can be
    required by interfaces.
    """

    ID = "ResourceBase"
    UPDATEABLE = False

    def __init__(self):
        pass

    def update(self):
        pass

    def info(self):
        return {}
