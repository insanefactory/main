from time import sleep
from lib.in_sane_factory.interface import Interface
from .action import Macro
import json
import os
import gc


class InSaneFactory:
    MODULE_REGISTRY = {}

    def __init__(self) -> None:
        self.INTERFACE_INSTANCES = {}
        self.resources = {}
        self.updateable_resources = []
        self.config = {}
        self.macros = {}
        self.startup_actions = []
        with open("resources.json") as resources:
            self.resources_settings = json.loads(resources.read())
        print("InSaneFactory innited!")
        print(id(self))
        print("[L] Free memory", gc.mem_free())

    def register(self, module):
        if module.ID in self.MODULE_REGISTRY.keys():
            raise Exception("Same ID module already registered.")
        self.MODULE_REGISTRY[module.ID] = module
        self.__register_resource__(module.init_resources(self.resources_settings))
        print("[L] Module registred: ", module.ID)
        print("[L] Free memory", gc.mem_free())
    
    def __register_resource__(self, resources: list):
        for resource in resources:
            self.resources[resource.ID] = resource
        self.updateable_resources.extend(filter(lambda r: r.UPDATEABLE, resources))

    def get_registered_modules(self):
        return self.MODULE_REGISTRY.keys()

    def get_registered_resources(self):
        return self.resources.keys()

    def _get_action(self, interface_id, type, args):
        if interface_id not in self.INTERFACE_INSTANCES:
            print(f"[L] Config load: Create action: interface {interface_id} not found")
            return None
        return self.INTERFACE_INSTANCES.get(interface_id).create_action(type, params=args)

    def __get_action__(self, action_config):
        makro_id = action_config.get("macro")
        if makro_id:
            return self.macros[makro_id]
        return self._get_action(action_config["interface"], action_config["type"], action_config["args"])

    def load_app(self):
        print("[L] Config load: Interfaces loading...")
        config_interfaces = self.config.get("interfaces")
        for interface_name in config_interfaces:
            print(f"[L] Config load: Resolving interface {interface_name}")
            interface_config = config_interfaces.get(interface_name)
            module_id = interface_config.get("module")
            print(f"[L] Config load: Resolving module {module_id}")
            module = self.MODULE_REGISTRY.get(module_id)
            if not module:
                print(f"[E] Config load: Module {module_id} not found")
                continue
            try:
                interface = module.create_interface(interface_config.get("interface"), interface_config.get("args", {}))
                self.__add_interface__(interface_name, interface)
            except Exception as e:
                print(f"[E] Config load: Resolve module: Interface {interface_name} can not initiated because of error:", e)
        print("[L] Config load: Interfaces loaded...")
        gc.collect()
        print("[L] Free memory", gc.mem_free())
        print("[L] Config load: Loading macros...")
        config_macros = self.config.get("macros")
        for macro_name in config_macros:
            macro_actions = config_macros[macro_name]
            makro = Macro()
            for action_config in macro_actions:
                action = self.__get_action__(action_config)
                if not action:
                    print(f"[W] Config load: Load macros: Action can not be created. Skipping action in macro {macro_name}.")
                    # TODO: Skip whole macro.
                    break
                makro.append_action(action)
            self.macros[macro_name] = makro
        print("[L] Config load: Loaded macros...")
        del(config_macros)
        if "macros" in self.config:
            del(self.config["macros"])
        gc.collect()
        print("[L] Free memory", gc.mem_free())
        print("[L] Config load: Loading triggers...")
        for interface_name in config_interfaces:
            print(f"[L] Config load: Resolving triggers for {interface_name}")
            if interface_name not in self.INTERFACE_INSTANCES.keys():
                print("[E] Config load: Resolving triggers: Interface not found.")
                continue
            interface_config = config_interfaces.get(interface_name)
            for trigger_config in interface_config.get("actions", []):
                action_config = trigger_config.get("action", {})
                action = self.__get_action__(action_config)
                if not action:
                    print(f"[E] Config load: Load triggers: Action can not be created. Skipping action in trigger {trigger_config['trigger_id']} of {interface_name}.")
                    # TODO: Skip whole macro.
                    break
                self.INTERFACE_INSTANCES[interface_name].set_trigger(trigger_config["trigger_id"], action)
        print("[L] Config load: Loaded triggers...")
        gc.collect()
        print("[L] Free memory", gc.mem_free())
        print("[L] Config load: Loading startup actions...")
        startup_action_config = self.config.get("startup_actions", [])
        for action_config in startup_action_config:
            action = self.__get_action__(action_config)
            self.startup_actions.append(action)
        print("[L] Config load: Loaded startup actions...")
        del(config_interfaces)
        del(self.config)
        self.config = {}
        gc.collect()
        print("[L] Free memory", gc.mem_free())


    def load_config(self):
        print("[L] Free memory", gc.mem_free())
        config_text = ""
        with open("config.json") as config_file:
            config_text = config_file.read()
        self.config = json.loads(config_text)
        print("[L] Config loaded\n[L] Free memory", gc.mem_free())

    def get_resources(self, ids):
        return {ID: self.resources.get(ID) for ID in ids}

    def __add_interface__(self, name, interface: Interface):
        self.INTERFACE_INSTANCES[name] = interface

    def load_modules(self):
        modules = os.listdir("/lib/")
        for module_name in modules:
            if module_name[:4] == "isf_":
                __import__(module_name)
        del(self.resources_settings)
        gc.collect()

    def run(self):
        for interface in self.INTERFACE_INSTANCES:
            print("[L] Running interfaces:", interface)
        for action in self.startup_actions:
            action.run()
        while True:
            for interface in self.INTERFACE_INSTANCES:
                self.INTERFACE_INSTANCES[interface].update()
            for resource in self.updateable_resources:
                resource.update()
            # sleep(0.1)

IN_SANE_FACTORY = InSaneFactory()

