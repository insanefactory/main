from .interface import Interface
from .action import Action


class Layer(Interface):
    ID = "layer"
    def __init__(self, layers:int, triggers:int):
        self.layers = [ [None for _ in range(triggers)] for l in range(layers)]
        self.triggers_number = triggers
        self.layers_number = layers
        self.active_layer = 0
        super().__init__()

    def check_layer(self, layer:int):
        return 0 <= layer < self.layers_number

    def check_trigger(self, trigger:int):
        return 0 <= trigger < self.triggers_number

    def set_trigger(self, id, action:Action):
        # TODO: Find better solution for this retyping. 
        layer, trigger =  id.split("_")
        layer = int(layer)
        trigger = int(trigger)
        if self.check_trigger(trigger) and self.check_layer(layer):
            self.layers[layer][trigger] = action

    def get_active_layer(self):
        return self.active_layer
    
    def set_active_layer(self, layer: int):
        self.active_layer = layer
    
    def trigger(self, trigger:int):
        if self.check_trigger(trigger):
            if self.layers[self.active_layer][trigger]:
                self.layers[self.active_layer][trigger].run()

    def create_action(self, type, params):
        if type == "action":
            action_id = params.get("id")
            if self.check_trigger(action_id):
                return LayerAction(self, action_id)
        elif type == "switch_to":
            layer_id = params.get("target")
            if self.check_layer(layer_id):
                return LayerSwitchAction(self, set_to=layer_id)
        elif type == "switch_by":
            switch_by = params.get("distance")
            rotate = params.get("rotateable", False)
            return LayerSwitchAction(self, switch_by=switch_by, rotate=rotate)

class LayerSwitchAction(Action):
    def __init__(self, interface: Layer, set_to:int = None, switch_by:int = None, rotate: bool = True):
        """
        Params:
            Interface: Layer interface
            set_to: ID of tagret layer
            switch_by: Number of layers to jump by
            rotate: Alow to circle throw layers
        """
        self.set_to = set_to
        self.switch_by = switch_by
        self.rotate = rotate
        # TODO: Check if target layer is in range of layers 
        # self.interface.layers_number
        super().__init__(interface)
    
    def run(self):
        if self.set_to is not None:
            self.interface.active_layer = self.set_to
        elif self.switch_by is not None:
            if self.rotate:
                self.interface.active_layer = (self.interface.active_layer + self.switch_by) % self.interface.layers_number
            


class LayerAction(Action):
    def __init__(self, interface: Layer, action_id:int):
        self.id = action_id
        super().__init__(interface)
    def run(self):
        self.interface.trigger(self.id)
